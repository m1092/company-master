from data import records
from districts import districts
import matplotlib.pyplot as pyplot


# Returns districts name againt number of companies registered in that district
def get_companies_by_district():
    registration_districts = {}

    for record in records[1:]:
        date = record[6]

        # Checks for the record matching with year 2015
        if (date.strip()[6:11] == '2015'):
            zip = record[12].strip()[-6:]

            # Checks if the last 6 characters represent a valid zip code
            if (zip.isdigit() and districts.get(zip) is not None):
                registration_districts[zip] = \
                    registration_districts.get(zip, 0) + 1

    # Create a list of districts from zip code that are found in the records
    districts_found = list(map(
        lambda zip: districts[zip],
        registration_districts.keys()
    ))

    return [districts_found, registration_districts.values()]


def plot_bar_graph():

    x_data, y_data = get_companies_by_district()
    pyplot.bar(x_data, y_data)

    # Customizing labels and title
    pyplot.xlabel("District")
    pyplot.ylabel("Number of Companies")
    pyplot.title("Company Registration by District: Bar Plot")
    pyplot.show()


if __name__ == '__main__':
    plot_bar_graph()
