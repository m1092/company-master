import csv

csvfile = open('Districts.csv', newline='', encoding='utf-8')

c = csv.reader(csvfile)

districts = {}

for row in c:
    districts[row[0]] = row[1]

csvfile.close()
