from data import records
import matplotlib.pyplot as pyplot


def get_grouped_data():
    years = {
        2011: 0, 2012: 1, 2013: 2, 2014: 3, 2015: 4,
        2016: 5, 2017: 6, 2018: 7, 2019: 8, 2020: 9
    }

    all_companies_by_year = [0] * 10
    business_companies_by_year = [0] * 10
    manufacturing_companies_by_year = [0] * 10
    construction_companies_by_year = [0] * 10
    other_companies_by_year = [0] * 10

    for record in records[1:]:
        date = record[6]

        if date != 'NA':
            year = int(date.strip()[6:11])

            if (years.get(year) is not None):
                index = years[year]
                all_companies_by_year[index] += 1

                activity = record[11]
                if (activity == "Real estate renting and business activities"):
                    business_companies_by_year[index] += 1

                elif (activity == "Manufacturing"):
                    manufacturing_companies_by_year[index] += 1

                elif (activity == "Construction"):
                    construction_companies_by_year[index] += 1

                else:
                    other_companies_by_year[index] += 1

    return [
        business_companies_by_year,
        manufacturing_companies_by_year,
        construction_companies_by_year,
        other_companies_by_year,
        all_companies_by_year
        ]


def plot_custom_bar_graph(plot_points, heights, custom_color, custom_width):

    width = 0.15

    pyplot.bar(
        [num + custom_width for num in plot_points],
        heights,
        width,
        color=custom_color
    )


def plot_grouped_bar_graph():

    x1_data, x2_data, x3_data, x4_data, x5_data = get_grouped_data()

    plot_points = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    plot_years = [
        '2011', '2012', '2013', '2014', '2015',
        '2016', '2017', '2018', '2019', '2020'
    ]

    # Creating 5 bar plots for each dataset using width .15
    plot_custom_bar_graph(plot_points, x1_data, 'red', -0.3)
    plot_custom_bar_graph(plot_points, x2_data, 'green', -0.15)
    plot_custom_bar_graph(plot_points, x3_data, 'blue', 0)
    plot_custom_bar_graph(plot_points, x4_data, 'yellow', 0.15)
    plot_custom_bar_graph(plot_points, x5_data, 'cyan', 0.3)

    # Creating a legend to describe color-code for principal activity
    pyplot.legend([
        "Real Estate and Business Companies",
        "Manufacturing Companies",
        "Construction Companies",
        "Other Companies",
        "All Companies"
    ])

    # Customizing labels, axis-values and title
    pyplot.xticks(plot_points, plot_years)
    pyplot.xlabel("Year of Registration and Principal Activity")
    pyplot.ylabel("Number of Companies")
    pyplot.title(
        "Company Registration by Year & Principal Activity: Grouped Bar Plot"
    )
    pyplot.show()


if __name__ == '__main__':
    plot_grouped_bar_graph()
