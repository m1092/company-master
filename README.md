# Company Master:: Maharashtra

## Aim
To convert raw open data into plots, that tell a story on the state of company registration in Maharashtra.

## How to get the data
* The dataset can be downloaded from [link](https://data.gov.in/resources/company-master-data-maharashtra-upto-28th-february-2019)
* This [resource](https://www.goldenchennai.com/pin-code/maharashtra-postal-code/) contains data of zip code and districts or directly download [csv-file](https://github.com/mihir784/districts-data).

## Pre-requisites
* Clone this repository into any directory.
* Download both data files and move them to the same directory.
* Names for raw data and districts data files should be "Data_Gov_Maharashtra.csv" and "Districts.csv", respectively.
* Create a virtual environment, using the command:
<br/>`python3 -m venv venv`
* Once the virtual environment is created, activate it using the command:
<br/>`source venv/bin/activate`

## Requirements
File **requirements.txt** contains all requirements and dependencies, to install them run the command:
<br/>`pip install -r requirements.txt`

## How to run the project
### Task 1: Histogram of Authorized Capital
* Plot a histogram on the "Authorized Capital" with the given intervals and labels as:
<br/>[<= 1L, 1L to 10L, 10L to 1Cr, 1Cr to 10Cr, > 10Cr].
* To plot the [graph](https://gitlab.com/m1092/company-master/-/blob/main/Graphs/Task_1.png), run command:
<br/>`python3 authorized_capital.py` .

### Task 2: Bar Plot of company registration by year
* Plot a bar plot of the number of company registrations v/s. the year with the year given in "Registration Date".
* To plot the [graph](https://gitlab.com/m1092/company-master/-/blob/main/Graphs/Task_2.png), run the command:
<br/>`python3 company_registration_by_year.py`.

### Task 3: Company registration in the year 2015 by the district
* Plot a bar plot of the number of company registrations in the year 2015 v/s. districts with the [district resource](https://github.com/mihir784/districts-data) and zip code mentioned in "Registration Address".
* To plot the [graph](https://gitlab.com/m1092/company-master/-/blob/main/Graphs/Task_3.png), run the command:
<br/>`python3 company_registraion_by_zip_code.py`.

### Task 4: Grouped Bar Plot
* Plot a Grouped Bar Plot for records collectively for:
  - Year of registration
  - Principal Business Activity
* Principal Business Activities are classified into these categories :
  1. Real Estate and Business Companies
  2. Manufacturing Companies
  3. Construction Companies
  4. Other Companies
  5. All Companies
* To plot the [graph](https://gitlab.com/m1092/company-master/-/blob/main/Graphs/Task_4.png), run the command:
<br/>`python3 grouped_bar_graph.py`.
<br/><br/>
**>** After closing all graphs, deactivate the virtual environment using "deactivate" command.

## Image files are in [Graphs](https://gitlab.com/m1092/company-master/-/tree/main/Graphs), for references.
