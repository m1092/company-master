from data import records
import matplotlib.pyplot as pyplot


# Creates and returns frequency distribution for Authorized Capital
def authorized_capital_fd():

    authorized_capital_class = []

    for record in records[1:]:
        authorized_capital = float(record[8])

        """
        Representing each record as a part of class interval such as:
        1 for (<1L), 2 for (1L to 10L) and so on for generating frequencies
        """
        if authorized_capital < 100000:
            authorized_capital_class.append(1)

        elif authorized_capital < 1000000:
            authorized_capital_class.append(2)

        elif authorized_capital < 10000000:
            authorized_capital_class.append(3)

        elif authorized_capital < 100000000:
            authorized_capital_class.append(4)

        elif authorized_capital >= 10000000:
            authorized_capital_class.append(5)

    return authorized_capital_class


def plot_histogram():

    frequency_distribution = authorized_capital_fd()
    pyplot.hist(frequency_distribution)

    # Customizing labels, axis-values and title
    custom_bins = [1, 2, 3, 4, 5]
    custom_labels = ["<1L", "1L to 10L", "10L to 1Cr", "1Cr to 10Cr", ">10Cr"]

    pyplot.xticks(custom_bins, custom_labels)
    pyplot.xlabel("Authorized Capital")
    pyplot.ylabel("Number of Companies")
    pyplot.title("Authorized Capital: Histogram")
    pyplot.show()


if __name__ == '__main__':
    plot_histogram()
