from data import records
import matplotlib.pyplot as pyplot


# Creates and returns years againt number of companies registered each year
def get_companies_by_year():
    registration_years = {}

    for record in records[1:]:
        date = record[6]

        if date != 'NA':
            year = int(date.strip()[6:11])

            if year > 1921 and year < 2022:
                registration_years[year] = registration_years.get(year, 0) + 1

    return [registration_years.keys(), registration_years.values()]


def plot_bar_graph():
    x_data, y_data = get_companies_by_year()
    pyplot.bar(x_data, y_data)

    # Customizing labels and title
    pyplot.xlabel("Year of Registration")
    pyplot.ylabel("Number of Companies")
    pyplot.title("Company Registration by Year: Bar Plot")
    pyplot.show()


if __name__ == '__main__':
    plot_bar_graph()
